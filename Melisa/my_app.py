from flask import Flask, render_template, request
from Crawler import WebCrawler

app = Flask(__name__)

@app.route("/")
def start():
    return render_template("main_page.html")

@app.route("/search", methods=["GET"])
def search():
    try:
        search_query = request.args.get("search", "Default Text")

        # Ensure the search query is not empty before calling WebCrawler
        if search_query:
            result = WebCrawler(search_query).search([search_query])
            return render_template("search_results.html", result=result, defaultSearchText=search_query)
        else:
            return render_template("error.html", error_message="Search query cannot be empty.")
    except Exception as e:
        return render_template("error.html", error_message=str(e))

if __name__ == "__main__":
    app.run(port=8000, debug=True)
