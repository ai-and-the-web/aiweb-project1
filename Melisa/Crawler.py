import os
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, urljoin
from whoosh.index import create_in, open_dir
from whoosh.fields import *
from whoosh.qparser import QueryParser

class WebCrawler:
    def __init__(self, start_url, index_dir = "indexdir"):
        """Initialize the crawler with a start URL."""
        self.start_url = start_url
        # the directory where the index is stored
        self.index_dir = index_dir
        # Use a set to keep track of visited URLS and to avoid duplicates
        self.visited_urls = set()
        # Store the index of the words
        self.index = self.initialize_index()
        

    def initialize_index(self):
        """Create or open the Whoosh index."""
        if not os.path.exists(self.index_dir):
            os.mkdir(self.index_dir)
            schema = Schema(url=ID(stored=True), content=TEXT)
            return create_in(self.index_dir, schema)
        else:
            return open_dir(self.index_dir)
        

    def crawl_and_index(self, url):
        """Crawl the given URL and index the words."""
        # If the URL has already been visited, return
        if url in self.visited_urls:
            return

        try:
            response = requests.get(url, timeout = 3)
            # Only parse and index the page if it is a HTML page and the response is OK
            if response.status_code == 200 and 'text/html' in response.headers.get('content-type', ''):
                # Parsing the HTML
                soup = BeautifulSoup(response.text, 'html.parser')
                # Index the words
                self.index_page(url, soup)

                # Find all links on the page and crawl them
                for link in soup.find_all('a', href = True):
                    # Join the URLs to get the absolute URL
                    next_url = urljoin(url, link['href'])
                    # Only crawl the URL if it is not already visited and if it is on the same domain
                    if self.is_same_domain(next_url):
                        self.crawl_and_index(next_url)
        except Exception as e:
            print(f"Error crawling {url}: {str(e)}")

    def index_page(self, url, soup):
        """Index the content of a page."""
        text_content = soup.get_text()
        with self.index.writer() as writer:
            writer.add_document(url=url, content=text_content)
        self.visited_urls.add(url)


    def is_same_domain(self, url):
        """Check if the start URL and the next URL are on the same domain."""
        start_domain = urlparse(self.start_url).netloc
        next_domain = urlparse(url).netloc
        return start_domain == next_domain

    def search(self, words):
        if not words:
            return []
        
        with self.index.searcher() as searcher:
            query_string = " AND ".join([f'content:{word}' for word in words])
            query = QueryParser("content", schema=self.index.schema).parse(query_string)
            results = searcher.search(query)
            result_urls = [result['url'] for result in results]
            return result_urls


# Usage example
start_url = "https://vm009.rz.uos.de/crawl/index.html"
crawler = WebCrawler(start_url)
crawler.crawl_and_index(start_url)

# Test the search functionality
search_words = ["platypus", "biology", "Australia"]
search_results = crawler.search(search_words)

print(f"Pages containing all words {search_words}:")
for result_url in search_results:
    print(result_url)