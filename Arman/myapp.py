
from flask import Flask, render_template, request
from crawler7 import search as run_crawler

app = Flask(__name__)

@app.route('/')
def start():
    return render_template('start.html')

@app.route('/search', methods=['GET'])
def search():
    search_query = request.args.get('search','Default Text')

    # Call the search function from crawler.py
    result = run_crawler(search_query)


    return render_template('results.html', result=result["search_results"],defaultSearchText=search_query)

if __name__ == '__main__':
    app.run(debug=True)
