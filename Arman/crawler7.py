import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse


def search(search_query):
    # Replace this with your actual crawler logic
    search_words = [search_query]

    initial_url = "https://vm009.rz.uos.de/crawl/index.html"

    def extract_links_and_text(url):
        response = requests.get(url, timeout = 3)

        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')

            page_text = soup.get_text()
            links = [urljoin(url, a['href']) for a in soup.find_all('a', href=True)]

            return links, page_text
        else:
            print(f"Failed to fetch {url}. Status code: {response.status_code}")
            return [], ""

    def is_same_domain(url, base_url):
        # Check if the given URL is in the same domain as the base URL
        return urlparse(url).netloc == urlparse(base_url).netloc

    def crawl_website(start_url, exclude_list=None, exclude_substring=None, max_links=None):
        index = {}
        visited_urls = set()
        to_visit = [start_url]
        crawled_links_count = 0

        while to_visit and (max_links is None or crawled_links_count < max_links):
            current_url = to_visit.pop(0)

            if current_url not in visited_urls and is_same_domain(current_url, start_url):
                if (exclude_list and current_url in exclude_list) or (
                        exclude_substring and exclude_substring in current_url):
                    print(f"Excluded {current_url}")
                    continue

                print(f"Crawling {current_url}")

                links, page_text = extract_links_and_text(current_url)
                index[current_url] = page_text
                to_visit.extend(links)
                visited_urls.add(current_url)

                crawled_links_count += 1
                print(f"Links crawled: {crawled_links_count}")

        return index

    def search_index(index, search_words):
        results = {}
        for url, content in index.items():
            for word in search_words:
                if word.lower() in content.lower():
                    results[word] = results.get(word, [])
                    results[word].append(url)

        return results

    # List of links to exclude
    exclude_list = []

    # Substring to exclude in links
    exclude_substring = ""

    # Maximum number of links to be crawled
    max_links_to_crawl = 30

    # Start crawling from the initial URL and get the index, excluding specified links
    index = crawl_website(initial_url, exclude_list=exclude_list, exclude_substring=exclude_substring,
                          max_links=max_links_to_crawl)

    # Search for the specified words in the index
    search_results = search_index(index, search_words)



    return {"search_results": search_results}





########################################################################